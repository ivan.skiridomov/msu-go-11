package main

import (
	//"fmt"
	"fmt"
	"sort"
	"strconv"
	"strings"
)

func main() {
	fmt.Print(IntSliceToString([]int{17, 23, 100500}))
}

// ReturnInt Return Int Value
func ReturnInt() int {
	return 1
}

// ReturnFloat Return Float
func ReturnFloat() float32 {
	return 1.1
}

// ReturnIntArray Return Int Array
func ReturnIntArray() [3]int {
	return [3]int{1, 3, 4}
}

// ReturnIntSlice Return Int Slice
func ReturnIntSlice() []int {
	return []int{1, 2, 3}
}

// IntSliceToString Int Slice To String
func IntSliceToString(values []int) string {
	textValues := []string{}
	for _, value := range values {
		textValues = append(textValues, strconv.Itoa(value))
	}
	result := strings.Join(textValues, "")
	return result
}

//MergeSlices Merge Slices
func MergeSlices(fSlice []float32, iSlice []int32) []int {
	result := []int{}

	for _, value := range fSlice {
		result = append(result, int(value))
	}

	for _, value := range iSlice {
		result = append(result, int(value))
	}

	return result
}

// GetMapValuesSortedByKey Get Map Values Sorted By Key
func GetMapValuesSortedByKey(input map[int]string) []string {

	var keys []int
	result := []string{}
	for k := range input {
		keys = append(keys, k)
	}
	sort.Ints(keys)

	for _, k := range keys {
		result = append(result, input[k])
	}
	return result
}
